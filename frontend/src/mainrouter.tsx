import { LoginForm } from 'component/loginform';
import { Publicites } from 'component/publicites';
import { PublicitesEditor } from 'component/publiciteseditor';
import { PubliciteView } from 'component/publicitesview';
import { BASE_HREF } from 'config.json';
import React from 'react';
import { Route, Switch } from 'react-router';
import { BrowserRouter, Link } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


interface Props { }
interface State { }

export class MainRouter extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
    }

    public render() {
        return <BrowserRouter basename={BASE_HREF}>
            <ToastContainer />
            <div className={'menu'}>
                <Link className={'elementMenu'} to='/publicites'>Publicite</Link>
            </div>

            <Switch>
                <Route path='/' component={LoginForm} />
                <Route path='/publicites/:publiciteId' component={PubliciteView} />
                <Route path='/publicites' component={Publicites} />
                <Route path='/publicites' component={PublicitesEditor} />
            </Switch>
        </BrowserRouter>;
    }
}
