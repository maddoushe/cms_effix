import React from 'react';
import { PublicitesEditor } from './publiciteseditor';

interface Props { }
interface State { }

export class Publicites extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {

        return <>
            <div className={'paragraphe'}>
                <h1>AVIS À LA CLIENTÈLE</h1>
                <p>
                    Comme le dit un dicton africain: «une personne sans culture est une personne sans patrie».
                    Créer des liens, une cohésion, un rapprochement, partager, promouvoir des talents,
                    revoir ceux qui sont déjà confirmés-nous le faisons à travers l’art et les échanges culturels.
                </p>
                <div  >
                    <PublicitesEditor />
                </div>
            </div>
        </>;
    }

}
