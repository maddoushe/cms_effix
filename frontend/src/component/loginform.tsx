import { Api } from 'api';
import React from 'react';
import { toast } from 'react-toastify';

interface Props { }
interface State { nomUtilisateur?: string; motDePasse?: string; }

export class LoginForm extends React.Component<Props, State> {
    private api = new Api;

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        return <div>
            <form onSubmit={this.login}>
                <div>
                    <label>Nom Utilisateur <input type='text' required={true} value={this.state.nomUtilisateur ?? ''}
                        onChange={e => { this.setState({ nomUtilisateur: e.target.value }); }} /></label>
                </div>
                <div>
                    <br /><label>Mot de Passe <input type='password' required={true} value={this.state.motDePasse ?? ''}
                        onChange={e => { this.setState({ motDePasse: e.target.value }); }} /></label>
                </div>
                <label>Connectez-vous
                    <input type='submit' />
                </label>
            </form>
        </div>;
    }

    private login = async (e: React.FormEvent) => {
        e.preventDefault();

        try {
            const utilsateur = await this.api.postGetJson('/auth/login', { nomUtilisateur: this.state.nomUtilisateur, motDePasse: this.state.motDePasse });
            console.log(utilsateur);
        } catch (error) {
            toast.error('nom d\'utilisateur ou mot de passe incorrect.');
        }
    };
}
