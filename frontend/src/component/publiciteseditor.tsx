import { Api } from 'api';
import { PubliciteModel } from 'common';
import React from 'react';
import Modal from 'react-modal';
import { PubliciteEditor } from './publiciteeditor';

interface Props { }
interface State {
    publicites?: PubliciteModel[];
    nomClient?: string;
    statutPub?: string;
    messagePub?: string;
    linkPub?: string;
    publiciteBeingEdited?: PubliciteModel;
}

export class PublicitesEditor extends React.Component<Props, State> {
    private api = new Api;

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public async componentDidMount() {
        const publicites = (await this.api.getJson('/publicite') as any[]).map(PubliciteModel.fromJSON);
        this.setState({ publicites });
    }

    public render() {
        const { publicites } = this.state;
        if (!publicites) { return 'Chargement...'; }

        return <>
            <Modal
                isOpen={this.state.publiciteBeingEdited !== undefined}
                onRequestClose={() => this.setState({ publiciteBeingEdited: undefined })}
                contentLabel='Publicite Edition'
            >
                {this.state.publiciteBeingEdited && <PubliciteEditor onRequestClose={() => this.setState({ publiciteBeingEdited: undefined, publicites })}
                    publicite={this.state.publiciteBeingEdited} />}

            </Modal>


            <div className={'publicite'}>
                <div className={'publiciteFlex'}>
                    <div className={'publiciteFlex'}>
                        <p>Bonjour User</p>
                        <button>Se deconnecter</button>
                    </div>
                    <button>Synchroniser</button>
                </div>

                <p>Liste des clients</p>
                <div className={'publiciteFlex'}>
                    <h3>Nom du client</h3>
                    <h3>Statut de la publicité</h3>
                    <h3>Éditeur de publicité</h3>
                </div>
                <div >
                    {publicites.map(publicite => <div className={'publiciteFlex'}
                        key={publicite.publiciteId}>

                        <p>{publicite.nomClient}</p>
                        <p>online/pas online </p>
                        <p className={'input'} onClick={() => this.setState({ publiciteBeingEdited: publicite })}>Lien</p>

                    </div>)}
                </div>
            </div>

            <form onSubmit={this.createPublicite} className={'publicite'}>
                <div className={'label'}>
                    <label>Nom Client <input className={'input'} required={true} value={this.state.nomClient ?? ''} onChange={(e: { target: { value: any; }; }) => this.setState({ nomClient: e.target.value })} /></label>
                    <br /><label>Statut Publicité <input className={'input'} required={true} value={this.state.statutPub ?? 'active'} onChange={(e: { target: { value: any; }; }) => this.setState({ statutPub: e.target.value })} /></label>
                    <br /><label>Message Publicité <input required={true} value={this.state.messagePub ?? ''} onChange={(e: { target: { value: any; }; }) => this.setState({ messagePub: e.target.value })} /></label>
                    <br /><label>Lien Publicité <input required={true} value={this.state.linkPub ?? ''} onChange={(e: { target: { value: any; }; }) => this.setState({ linkPub: e.target.value })} /></label>
                    <br /><button>Créer</button>
                </div>
            </form>
        </>;
    }

    private createPublicite = async (e: React.FormEvent) => {
        e.preventDefault();
        const publicite = { nomClient: this.state.nomClient, statutPub: this.state.statutPub, messagePub: this.state.messagePub, linkPub: this.state.linkPub };
        const createPublicite = PubliciteModel.fromJSON(await this.api.postGetJson('/publicite', publicite));
        this.state.publicites!.push(createPublicite);
        this.setState({ publicites: this.state.publicites, nomClient: '', statutPub: '', messagePub: '', linkPub: '' });
    };
}
