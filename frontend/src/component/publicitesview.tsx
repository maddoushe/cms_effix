import { Api } from 'api';
import { PubliciteModel } from 'common';
import React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';

interface Props extends RouteComponentProps<{ publiciteId: string; }> { }
interface State {
    publicite?: PubliciteModel | null;
    currentPubliciteId?: number;
}

export class PubliciteView extends React.Component<Props, State> {
    public componentDidMount = this.componentDidUpdate;
    private api = new Api;

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public async componentDidUpdate() {
        const newCurrentPubliciteId = parseInt(this.props.match.params.publiciteId);
        if (this.state.currentPubliciteId === newCurrentPubliciteId) { return; }
        try {
            const publicite = PubliciteModel.fromJSON(await this.api.getJson(`/publicite/${newCurrentPubliciteId}`));
            this.setState({ publicite, currentPubliciteId: newCurrentPubliciteId });

        } catch {
            this.setState({ publicite: null, currentPubliciteId: newCurrentPubliciteId });
        }
    }

    public render() {
        const currentPubliciteId = parseInt(this.props.match.params.publiciteId);
        const { publicite } = this.state;
        if (publicite === undefined) { return 'Chargement...'; }
        if (publicite === null) {
            return this.renderLinks(currentPubliciteId);
        }

        return <>
            {this.props.match.params.publiciteId}
            {this.renderLinks(currentPubliciteId)}
            <div style={{ border: '1px solid' }} key={publicite.publiciteId}>
                <p>Nom Client: {publicite.nomClient}</p>
                <p>Statut Publicité: {publicite.statutPub}</p>
                <p>Message Publicité: {publicite.messagePub}</p>
                <p>Lien Publicité: {publicite.linkPub}</p>
            </div>
        </>;
    }

    public renderLinks(currentPubliciteId: number) {
        return <>
            <Link to={`/publicites/${currentPubliciteId - 1}`}>Précédent</Link>{' '}
            <Link to={`/publicites/${currentPubliciteId + 1}`}>Suivant</Link>
        </>;
    }
}
