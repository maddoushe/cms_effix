import { Api } from 'api';
import { PubliciteModel } from 'common';
import React from 'react';

interface Props { publicite: PubliciteModel; onRequestClose(): void; }
interface State { nomClient: string; statutPub: boolean; messagePub: string; linkPub: string; }

export class PubliciteEditor extends React.Component<Props, State> {
    private api = new Api;

    constructor(props: Props) {
        super(props);

        this.state = {
            nomClient: props.publicite.nomClient,
            statutPub: props.publicite.statutPub,
            messagePub: props.publicite.messagePub,
            linkPub: props.publicite.linkPub
        };
    }

    public render() {
        return <>
            <form onSubmit={this.updatePublicite}>
                <div>
                    <h2>Bonjour Utilisateur</h2>
                    <button>Sycnchroniser les clients</button>
                </div>
                <div>
                    <h2>Nom: ....</h2>
                    <h2> Statut de la publicité: ....</h2>
                </div>
                <div>
                    <div>
                        <p>300 x 250</p>
                        <img src='' alt='' />
                    </div>
                    <div>
                        <p>200 x 200</p>
                        <img src='' alt='' />
                    </div>
                </div>


                <input required={true} value={this.state.messagePub ?? ''} onChange={e => this.setState({ messagePub: e.target.value })} />
                <input required={true} value={this.state.linkPub ?? ''} onChange={e => this.setState({ linkPub: e.target.value })} />


                <div>
                    <h2>les images(1 au choix)</h2>
                    {/*
                    <form onChange={this.onChangeValue}>
                        <input type='radio' className={'activite'} value='<img src="" alt=""/>' />111
                        <input type='radio' className={'activite'} value='<img src="" alt=""/>' />111
                        <input type='radio' className={'activite'} value='<img src="" alt=""/>' />111
                        <input type='radio' className={'activite'} value='<img src="" alt=""/>' />111


                    </form> */}
                </div>

                <button type='submit'>Sauvegarder</button>
                <button type='button' onClick={this.props.onRequestClose}>Annuler</button>
            </form>
        </>;
    }

    private updatePublicite = async (e: React.FormEvent) => {
        e.preventDefault();

        this.props.publicite.nomClient = this.state.nomClient;
        this.props.publicite.statutPub = this.state.statutPub;
        this.props.publicite.messagePub = this.state.messagePub;
        this.props.publicite.linkPub = this.state.linkPub;

        await this.api.put('/publicite', this.props.publicite.publiciteId, this.props.publicite);
        this.props.onRequestClose();
    };

}
