
export class PubliciteModel {
    public publiciteId: number;
    public nomClient: string;
    public statutPub: boolean;
    public messagePub: string;
    public linkPub: string;

    public static fromJSON(jsonPubliciteModel: PubliciteModel) {
        const publiciteModel = new PubliciteModel;
        Object.assign(publiciteModel, jsonPubliciteModel);
        return publiciteModel;
    }
}
