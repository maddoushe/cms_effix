import { PubliciteModel, UserModel } from 'common';

declare global {
    module Express {
        interface Request {
            publicite: PubliciteModel;
        }
        interface User extends UserModel { }
    }

}
