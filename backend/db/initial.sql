
DROP TABLE IF EXISTS `publicite`;
CREATE TABLE `publicite` (
  `publiciteId` INT NOT NULL AUTO_INCREMENT,
  `nomClient` varchar(255) NOT NULL,
  `statutPub` TINYINT(4) NOT NULL DEFAULT '0',
  `messagePub` varchar(255) NOT NULL,
  `linkPub` varchar(255) NOT NULL,
  PRIMARY KEY (`publiciteId`)
);

LOCK TABLES `publicite` WRITE;
INSERT INTO `publicite` VALUES (1,'Client 1','0','promotion','lien url');
UNLOCK TABLES

ALTER TABLE `publicite`
  MODIFY `publiciteId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Structure de la table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `sessionId` int(11) NOT NULL,
  `expires` datetime NOT NULL,
  `data` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `sessions`
  ADD PRIMARY KEY (`sessionId`);


--
-- Structure de la table `role`
--

CREATE TABLE `role` (
  `userId` int(11) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`userId`, `role`) VALUES
(1, 'admin'),
(1, 'user'),
(2, 'user');


ALTER TABLE `role`
  ADD PRIMARY KEY (`userId`,`role`);

ALTER TABLE `role`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `userId` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`userId`, `username`, `password`) VALUES
(2, 'tp3.2', '$2b$16$D8d/ZRM6Nl9Vb7TrcaokO.KBhGZ3UK1dGPZQRqIspZqwAaFvhx3JK');

ALTER TABLE `user`
  ADD PRIMARY KEY (`userId`);

ALTER TABLE `user`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;
