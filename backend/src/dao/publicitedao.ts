import { PubliciteModel } from 'common';
import { DBProvider } from '../dbprovider';

export class PubliciteDAO {
    private knex = DBProvider.getKnexConnection();

    public async createPublicite(Publicite: PubliciteModel) {
        const { nomClient, statutPub, messagePub, linkPub } = Publicite;
        const [publiciteId] = await this.knex('publicite').insert({ nomClient, statutPub, messagePub, linkPub });
        return publiciteId;
    }

    public async getPublicite(publiciteId: number | string) {
        const publicite = await this.knex('publicite').first('*').where({ publiciteId });
        if (!publicite) { return null; }
        return PubliciteModel.fromJSON(publicite);
    }

    public async updatePublicite(Publicite: PubliciteModel) {
        const { publiciteId, nomClient, statutPub, messagePub, linkPub } = Publicite;
        await this.knex('publicite').update({ nomClient, statutPub, messagePub, linkPub }).where({ publiciteId });
    }

    public async deletePublicite(publiciteId: number) {
        await this.knex('publicite').delete().where({ publiciteId });
    }

    public async getPublicites() {
        const publicites = await this.knex('publicite').select('*');
        return publicites.map(PubliciteModel.fromJSON);
    }
}
